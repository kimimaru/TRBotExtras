# TRBotExtras

A repository containing custom commands, custom routines, and additional resources for [TRBot](https://codeberg.org/kimimaru/TRBot) that are out of scope for the core repository.

This is essentially a community repository - please open a PR if you want to contribute!

Since custom commands and routines directly utilize TRBot code, they are licensed under the same license as [TRBot](https://codeberg.org/kimimaru/TRBot/src/branch/master/LICENSE), AGPLv3.
