/* ShowCPUTempsCommand
   - Displays CPU temps in text to help diagnose performance issues while streaming
   - Currently configured for GNU/Linux systems - patches are welcome for other platforms!
   - Requires lm_sensors to be installed on the host machine
*/

using System;
using System.IO;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TRBot.Commands;
using TRBot.Permissions;
using TRBot.Utilities;

public class ShowCPUTempsCommand : BaseCommand
{
    public ShowCPUTempsCommand()
    {
    
    }
    
    public override void ExecuteCommand(EvtChatCommandArgs args)
    {
        string argStr = args.Command.ArgumentsAsString;
        
        if (string.IsNullOrEmpty(argStr) == false)
        {
            QueueMessage(args.ServiceName, "This command doesn't take any arguments.");
            return;
        }
        
        //Start in another task
        Task.Run(() => StartSensorsProcess(args.ServiceName));
    }
    
    private async Task StartSensorsProcess(string serviceName)
    {
        string standardOut = string.Empty;
        
        try
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            
            startInfo.FileName = "sensors";
            
            //Redirect standard output and error
            startInfo.RedirectStandardError = true;
            startInfo.RedirectStandardOutput = true;
            
            using (Process p = Process.Start(startInfo))
            {
                await p.WaitForExitAsync();
                
                //QueueMessage(serviceName, $"EXIT CODE = {p.ExitCode}"); 
                
                //sensors threw an error, so say what it is
                if (p.ExitCode != 0)
                {
                    //Read standard error
                    string stdErr = await p.StandardError.ReadToEndAsync();
                    
                    QueueMessage(serviceName, stdErr);
                    
                    return;
                }
                
                standardOut = await p.StandardOutput.ReadToEndAsync();
            }
        }
        catch (Exception e)
        {
            QueueMessage(serviceName, $"Error obtaining temps - {e.Message}");
            return;
        }
        
        string[] outputArr = standardOut.Split(Environment.NewLine, StringSplitOptions.RemoveEmptyEntries)
            .Where(s => s.StartsWith("Core "))
            .Select(s => s.Replace(Environment.NewLine, string.Empty))
            .Select(s => Helpers.ReplaceAllWhitespaceWithSpace(s))
            .Select(s => (s, s.IndexOf('(')))
            .Where((s, i) => i >= 0)
            .Select((s, i) => s.s.Remove(s.Item2 - 1))
            .ToArray();
        
        if (outputArr.Length == 0)
        {
            QueueMessage(serviceName, "Unable to obtain temps - No output available");
            return;
        }
        
        string output = string.Join(" | ", outputArr);
        
        QueueMessage(serviceName, output, " | ");
    }
}

return new ShowCPUTempsCommand();
