﻿/* ChatbotCommand
 *   - Allows interacting with a Chatterbot instance
 *   - See this guide for more information: https://codeberg.org/kimimaru/TRBot/src/branch/develop/Wiki/Setup-Chatterbot.md
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Pipes;
using System.Net.Sockets;
using TRBot.Commands;
using TRBot.Connection;
using TRBot.Permissions;
using TRBot.Data;
using TRBot.Utilities.Logging;

/// <summary>
/// Allows users to interact with an external chatbot.
/// </summary>
public sealed class ChatbotCommand : BaseCommand
{
    //These settings need to be added to the database
    private const string CHATBOT_ENABLED = "chatbot_enabled";
    private const string CHATBOT_SOCKET_HOSTNAME = "chatbot_socket_hostname";
    private const string CHATBOT_SOCKET_PORT = "chatbot_socket_port";
    
    //Add this permission ability to the database
    private const string CHATBOT_ABILITY = "chatbot";
    
    /// <summary>
    /// The response timeout for the chatbot.
    /// </summary>
    private const int RESPONSE_TIMEOUT = 1000;

    private string UsageMessage = "Usage: \"prompt or question (string)\"";

    public ChatbotCommand()
    {

    }

    public override void ExecuteCommand(EvtChatCommandArgs args)
    {
        //Check if the chatbot is enabled
        long chatbotEnabled = DatabaseMngr.GetSettingInt(CHATBOT_ENABLED, 0L);

        if (chatbotEnabled != 1)
        {
            QueueMessage(args.ServiceName, "The host is not currently using a chatbot!");
            return;
        }
        
        using (BotDBContext context = DatabaseMngr.OpenContext())
        {
            //Check if the user has the ability to chat with the chatbot
            User user = context.GetUserByExternalID(args.Command.UserMessage.UserExternalID);

            if (user != null && user.HasEnabledAbility(CHATBOT_ABILITY) == false)
            {
                QueueMessage(args.ServiceName, "You do not have the ability to chat with the chatbot.");
                return;
            }
        }

        string question = args.Command.ArgumentsAsString;
        
        //The user needs to send a prompt to the bot
        if (string.IsNullOrEmpty(question) == true)
        {
            QueueMessage(args.ServiceName, UsageMessage);
            return;
        }

        string chatbotHostName = DatabaseMngr.GetSettingString(CHATBOT_SOCKET_HOSTNAME, "127.0.0.1");
        int chatbotPort = (int)DatabaseMngr.GetSettingInt(CHATBOT_SOCKET_PORT, 7444);
        
        try
        {
            Logger.Debug($"Full chatbot address: {chatbotHostName}:{chatbotPort}");

            //Set up the stream by connecting over TCP
            using (TcpClient client = new TcpClient(chatbotHostName, chatbotPort))
            {
                using (NetworkStream nStream = client.GetStream())
                {
                    //Send the input to ChatterBot
                    using (BinaryWriter promptWriter = new BinaryWriter(nStream ))
                    {
                        using (BinaryReader responseReader = new BinaryReader(nStream))
                        {
                            //Get a byte array
                            byte[] byteBuffer = System.Text.Encoding.ASCII.GetBytes(question);

                            //Send the data to the socket
                            promptWriter.Write((uint)byteBuffer.Length);
                            promptWriter.Write(byteBuffer);

                            //Get the data back from the socket
                            uint responseLength = responseReader.ReadUInt32();

                            Logger.Debug($"Response length: {responseLength}");

                            string response = new string(responseReader.ReadChars((int)responseLength));

                            Logger.Debug($"Received response: {response}");

                            //Output the response
                            QueueMessage(args.ServiceName, response);
                        }
                    }
                }
            }
        }
        catch (Exception exc)
        {
            QueueMessage(args.ServiceName, $"Error with sending chatbot reply. {exc.Message} - Please check the \"{CHATBOT_SOCKET_HOSTNAME}\" and \"{CHATBOT_SOCKET_PORT}\" settings in the database. Also ensure your ChatterBot instance is running!", Serilog.Events.LogEventLevel.Warning);
        }
    }
}

return new ChatbotCommand();