/* ViewstateModified
   - Displays the last time a given savestate was modified - useful for players to see the latest state
   - Designed for RetroArch savestates, but may work for other emulation software
*/

using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TRBot.Permissions;
using TRBot.Utilities;

public class ViewstateModifiedCommand : BaseCommand
{
    public ViewstateModifiedCommand()
    {
    
    }
    
    public override void ExecuteCommand(EvtChatCommandArgs args)
    {
        const bool appendNumOnFirstState = false;
        const int startModifier = -1;

        //Fill out with base savestate path
        const string basePath = @"";
        
        List<string> arguments = args.Command.ArgumentsAsList;
        
        if (arguments.Count != 1)
        {
            QueueMessage(args.ServiceName, "Usage: state number");
            return;
        }
        
        if (int.TryParse(arguments[0], out int stateNum) == false)
        {
            QueueMessage(args.ServiceName, "Invalid state number!");
            return;
        }
        
        string filePath = basePath;

        if (stateNum != 1 || appendNumOnFirstState == true)
        {
            filePath += (startModifier + stateNum).ToString();
        }

        if (File.Exists(filePath) == false)
        {   
            QueueMessage(args.ServiceName, $"No state for state number {stateNum} exists!");
            return;
        }
        
        DateTime lastFileWrite = default;
        
        try
        {
            lastFileWrite = File.GetLastWriteTimeUtc(filePath);
        }
        catch (Exception e)
        {
            QueueMessage(args.ServiceName, $"Error accessing file: {e.Message}");
            return;
        }
        
        TimeSpan diff = DateTime.UtcNow - lastFileWrite;
        
        string messageExt = "- ";
        
        if (diff.Days > 0)
        {
            messageExt += $"{diff.Days} days, ";
        }
        
        if (diff.Hours > 0)
        {
            messageExt += $"{diff.Hours} hours, ";
        }
        
        if (diff.Minutes > 0)
        {
            messageExt += $"{diff.Minutes} minutes, ";
        }
        
        messageExt += $"{diff.Seconds} seconds ago";
        
        QueueMessage(args.ServiceName, $"State {stateNum} was last modified on {lastFileWrite} (UTC) {messageExt}!");
    }
}

return new ViewstateModifiedCommand();
