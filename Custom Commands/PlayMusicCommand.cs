/* PlayMusicCommand
   - Allows playing MIDI music with alda
    - alda must be installed: https://github.com/alda-lang/alda
   - To charge credits per use, add a "music_credit_cost" setting in the database with the ValueInt as the number of credits to charge
*/

using System.IO;
using System.Linq;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using TRBot.Commands;
using TRBot.Permissions;
using TRBot.Utilities;
using Microsoft.EntityFrameworkCore;

public class PlayMusicCommand : BaseCommand
{
    private const string CREDIT_COST_SETTING = "music_credit_cost";
    
    public PlayMusicCommand()
    {
    
    }
    
    public override void ExecuteCommand(EvtChatCommandArgs args)
    {
        string argStr = args.Command.ArgumentsAsString;
        
        if (string.IsNullOrEmpty(argStr) == true)
        {
            QueueMessage(args.ServiceName, "Usage: alda code - Alda is a language for writing music. See: https://alda.io/tutorial/");
            return;
        }
        
        long creditCost = DatabaseMngr.GetSettingInt(CREDIT_COST_SETTING, 0L);
        
        string userExternalID = args.Command.UserMessage.UserExternalID;
        
        //If it costs credits, check if the user has enough and is opted into bot stats
        if (creditCost > 0L)
        {
            string creditsName = DatabaseMngr.GetCreditsName();
            
            string creditsPluralized = creditsName.Pluralize(creditCost);
            
            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTrackingWithIdentityResolution;
                
                User user = context.Users
                    .Include(u => u.Stats)
                    .FirstOrDefault(u => u.ExternalID == userExternalID);
                
                if (user != null && user.HasConsentToDataOption(UserDataConsentOptions.Stats) == false)
                {
                    QueueMessage(args.ServiceName, $"Playing music costs {creditCost} {creditsPluralized}, but you're opted out of stats, so you cannot play music!");
                    return;
                }
                
                if (user != null && user.Stats.Credits < creditCost)
                {
                    QueueMessage(args.ServiceName, $"You do not have enough {creditsPluralized} to play music! Playing music costs {creditCost} {creditsPluralized}!");
                    return;
                }
            }
        }
        
        //Play it in another task
        Task.Run(() => StartMusicProcess(args.ServiceName, argStr, args.Command.UserMessage.Username, userExternalID, creditCost));
    }
    
    private async Task StartMusicProcess(string serviceName, string musicArgs, string userName, string userExternalID, long creditCost)
    {
        try
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            
            startInfo.FileName = "alda";
            startInfo.Arguments = $"play -c \"{musicArgs}\"";
            
            //Redirect standard error so alda outputs syntax errors if the string doesn't parse
            startInfo.RedirectStandardError = true;
            
            using (Process p = Process.Start(startInfo))
            {
                await p.WaitForExitAsync();
                
                //QueueMessage(serviceName, $"EXIT CODE = {p.ExitCode}"); 
                
                //alda threw an error, so say what it is
                if (p.ExitCode != 0)
                {
                    //Read standard error
                    string stdErr = await p.StandardError.ReadToEndAsync();
                    
                    QueueMessage(serviceName, $"(Alda Syntax Error) {stdErr}");
                    
                    return;
                }
            }
        }
        catch (Exception e)
        {
            QueueMessage(serviceName, $"Error playing music - {e.Message}");
            return;
        }
        
        //The music successfully played at this point, so subtract credits if it costs any
        if (creditCost > 0L)
        {
            using (BotDBContext context = DatabaseMngr.OpenContext())
            {
                User user = context.GetUserByExternalID(userExternalID);
                
                //Subtract credits and save
                user.Stats.Credits -= creditCost;
                
                //Since this is asynchronous, the user could have fewer credits than the credit cost if it took long enough to play the music
                //In that event, simply set their credits to 0 to avoid them having negative credits
                if (user.Stats.Credits < 0L)
                {
                    user.Stats.Credits = 0L;
                }
                
                context.SaveChanges();
            }
        
            string creditsName = DatabaseMngr.GetCreditsName();
        
            QueueMessage(serviceName, $"{userName} spent {creditCost} {creditsName.Pluralize(creditCost)} to play music!");
        }
    }
}

return new PlayMusicCommand();
