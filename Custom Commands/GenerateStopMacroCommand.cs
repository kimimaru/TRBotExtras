using System.Linq;
using System.Text;
using TRBot.Commands;
using TRBot.Permissions;

public class GenerateStopMacroCommand : BaseCommand
{
    public GenerateStopMacroCommand()
    {
        
    }
    
    public override void ExecuteCommand(EvtChatCommandArgs args)
    {
        List<string> arguments = args.Command.ArgumentsAsList;
        
        if (arguments.Count > 1)
        {
            QueueMessage(args.ServiceName, "Usage: \"milliseconds for each input (int) (optional)\"");
            return;
        }
        
        int msValue = 17;
        if (arguments.Count > 0)
        {
            if (int.TryParse(arguments[0], out msValue) == false)
            {
                QueueMessage(args.ServiceName, "Invalid millisecond argument.");
                return;
            }
        }
        
        string msValStr = msValue.ToString();
        
        int lastConsole = (int)DatabaseMngr.GetSettingInt(SettingsConstants.LAST_CONSOLE, 1L);
        
        StringBuilder strBuilder = null;
        
        using (BotDBContext context = DatabaseMngr.OpenContext())
        {
            GameConsole curConsole = context.Consoles.FirstOrDefault(c => c.ID == lastConsole);
            if (curConsole == null)
            {
                QueueMessage(args.ServiceName, $"Current console with ID {lastConsole} is somehow null!?");
                return;
            }
        
            strBuilder = new StringBuilder(256);
        
            for (int i = 0; i < curConsole.InputList.Count; i++)
            {
                InputData inputData = curConsole.InputList[i];
        
                //Only add valid, non-blank inputs that everyone has access to
                if (string.IsNullOrEmpty(inputData.Name) == true || inputData.InputType == InputTypes.Blank || inputData.Level != (int)PermissionLevels.User)
                {
                    continue;
                }
        
                strBuilder.Append('-').Append(inputData.Name).Append(msValStr).Append("ms").Append('+');
            }
        
            if (strBuilder.Length > 1)
            {
                strBuilder.Remove(strBuilder.Length - 1, 1);
            }    
        }
        
        string stopMacroName = "#stop";
        string stopMacroVal = strBuilder.ToString();
        
        using (BotDBContext context = DatabaseMngr.OpenContext())
        {
            InputMacro macro = context.Macros.FirstOrDefault(m => m.MacroName == stopMacroName);
            if (macro == null)
            {
                macro = new InputMacro(stopMacroName, stopMacroVal);
                context.Macros.Add(macro);
        
                QueueMessage(args.ServiceName, $"Generated the {stopMacroName} macro!");
            }
            else
            {
                macro.MacroValue = stopMacroVal;
                QueueMessage(args.ServiceName, $"Updated the {stopMacroName} macro!");
            }
            
            context.SaveChanges();
        }
    }
}

return new GenerateStopMacroCommand();
