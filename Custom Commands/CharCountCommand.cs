/* CharCountCommand
   - Simple character count, useful for checking message length for services with set limits (Ex. Twitch)
*/

using System;
using TRBot.Commands;

public class CharCountCommand : BaseCommand
{
    public CharCountCommand()
    {
        
    }

    public override void ExecuteCommand(EvtChatCommandArgs args)
    {
        QueueMessage(args.ServiceName, args.Command.ArgumentsAsString.Length.ToString());
    }
}

return new CharCountCommand();
