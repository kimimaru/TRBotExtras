/* RestartRetroArchCommand
   - Restarts RetroArch with the given core and ROM - useful if it crashes
   - Currently configured for GNU/Linux systems - patches are welcome for other platforms!
*/

using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using TRBot.Connection;
using TRBot.Commands;
using TRBot.Permissions;
using TRBot.Routines;
using TRBot.Utilities;
using TRBot.Utilities.Logging;

public class RestartRetroArchCommand : BaseCommand
{   
    //Fill in with the core directory
    private const string BASE_CORE_DIR = @"";
    private const string CORE_NAME = @"";

    //Fill in with the ROM directory
    private const string BASE_ROM_DIR = @"";
    private const string ROM_NAME = @"";
    
    public RestartRetroArchCommand()
    {

    }
    
    public override void ExecuteCommand(EvtChatCommandArgs e)
    {
        Task.Run(() => RunCommand());
    }
    
    private async Task RunCommand()
    {
        //Kill RetroArch and wait
        using (Process pKill = Process.Start("/usr/bin/pkill", "retroarch"))
        {
            await Task.Delay(2000);
        }
        
        //Run RetroArch with the given core and ROM
        using (Process game = Process.Start("/usr/local/bin/retroarch", $"-L \"{BASE_CORE_DIR}{CORE_NAME}\" \"{BASE_ROM_DIR}{ROM_NAME}\""))
        {
            await Task.Delay(2000);
        }
    }
}

return new RestartRetroArchCommand();
