/* GameRotationRoutine
   - A routine that switches games every hour (by default) on RetroArch
   - RetroArch must be installed
   - Define the games and cores you want to run in CoreNames and GameNames below
   - The "game_rotation_index" and "last_game_rotation_time" settings are automatically added to the database if they don't exist
   - Currently configured for GNU/Linux systems - patches are welcome for other platforms!

   - Take your time to read the comments, as this is quite an involved routine
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Diagnostics;
using TRBot.Input.Consoles;
using TRBot.Utilities;
using TRBot.Routines;
using TRBot.Connection;
using TRBot.Data;

public class GameRotationRoutine : BaseRoutine
{
    //Configure the switch time here, in milliseconds
    //It defaults to 3600000 milliseconds, or every hour
    private const double SWITCH_TIME_MS = 1000d * 60d * 60d;
    
    private const string SCRIPT_TO_RUN = "/usr/local/bin/retroarch";
    
    private const string ROTATION_INDEX_SETTING = "game_rotation_index"; 
    private const string LAST_ROTATION_TIME_SETTING = "last_game_rotation_time";
    
    //Define the base core directory here (Ex. "/home/user/.config/retroarch/cores/")
    private const string BASE_CORE_DIR = @"";
    
    //Define the base ROM directory here (Ex. "/home/user/.config/retroarch/roms/")
    private const string BASE_ROM_DIR = @"";
    
    //Define each core to rotate through here!
    //The game you want to play has to have an associated core (Ex. "genesis_plus_gx_libretro.so" for loading Genesis Plus GX)
    private readonly string[] CoreNames = new string[]
    {
        BASE_CORE_DIR + @"",
    };
    
    //Define each ROM to rotate through here!
    //This is the actual ROM name (Ex. "Ristar (UE) [!].bin")
    private readonly string[] GameNames = new string[]
    {
        BASE_ROM_DIR + @"",
    };
    
    //These are the raw game names - in other words, the display names
    //TRBot will output a message saying it's loading up a new game when it switches
    //This is the name viewers will see! 
    private readonly string[] RawGameNames = new string[]
    {
        //Ex. "Super Mario World (SNES)"
        @"",
    };
    
    //These are the consoles to switch TRBot to for inputs when the game changes
    //Ex. For a SNES game, this can be set to the "snes" console
    //You can use any console in the database, including custom ones if you have any
    private readonly string[] ConsoleNames = new string[]
    {
        @"snes",
        @"gbc",
        @"genesis",
        @"ps2",
        @"snes",
    };
    
    //This is the name of a text file to save the game name into
    //Useful for displaying this on OBS
    private const string GAME_TEXT_FILE_NAME = @"";

    //This is the name of a text file to save the last time the game switched
    //Useful for displaying this on OBS
    private const string START_TIME_TEXT_FILE_NAME = @"";

    private DateTime LastRotTime = DateTime.UnixEpoch;
    
    public GameRotationRoutine()
    {

    }

    public override void Initialize()
    {
        base.Initialize();
        
        using (BotDBContext context = DatabaseMngr.OpenContext())
        {
            Settings lastRotTimeSetting = context.SettingCollection.FirstOrDefault(s => s.Key == LAST_ROTATION_TIME_SETTING);
        
            //Add the setting if it doesn't exist
            if (lastRotTimeSetting == null)
            {
                lastRotTimeSetting = new Settings(LAST_ROTATION_TIME_SETTING, DateTime.UtcNow.ToConsistentString(), 0L);
                context.SettingCollection.Add(lastRotTimeSetting);
                context.SaveChanges();
            }
            
            if (DateTime.TryParse(lastRotTimeSetting.ValueStr, out LastRotTime) == false)
            {
                LastRotTime = DateTime.UnixEpoch;
            }
        }
    }
    
    public override void UpdateRoutine(in DateTime currentTimeUTC)
    {
        TimeSpan diff = currentTimeUTC - LastRotTime;
        
        if (diff.TotalMilliseconds < SWITCH_TIME_MS)
        {
            return;
        }
        
        //Update the time in the database
        using (BotDBContext context = DatabaseMngr.OpenContext())
        {
            Settings lastRotTimeSetting = context.GetSetting(LAST_ROTATION_TIME_SETTING);
        
            lastRotTimeSetting.ValueStr = currentTimeUTC.ToConsistentString();
            context.SaveChanges();
        }
        
        LastRotTime = currentTimeUTC;
        
        //Kill RetroArch
        using (Process pKill = Process.Start("/usr/bin/pkill", "retroarch"))
        {
            
        }
        
        int curIndex = 0;
        
        using (BotDBContext context = DatabaseMngr.OpenContext())
        {
            Settings gameRotSetting = context.SettingCollection.FirstOrDefault(s => s.Key == ROTATION_INDEX_SETTING);
        
            //Add the setting if it doesn't exist
            if (gameRotSetting == null)
            {
                gameRotSetting = new Settings(ROTATION_INDEX_SETTING, string.Empty, 0L);
                context.SettingCollection.Add(gameRotSetting);
                context.SaveChanges();
            }
            
            //Clamp to the max value
            if (gameRotSetting.ValueInt >= GameNames.Length)
            {
                gameRotSetting.ValueInt = 0;
                context.SaveChanges();
            }
            
            curIndex = (int)gameRotSetting.ValueInt;
        }
        
        string curCore = CoreNames[curIndex];
        string curGame = GameNames[curIndex];
        string consoleName = ConsoleNames[curIndex];
        
        TimeSpan switchTimespan = TimeSpan.FromMilliseconds(SWITCH_TIME_MS);
        DateTime nextGameTimeUTC = currentTimeUTC + switchTimespan;
        
        //Switch console
        using (BotDBContext context = DatabaseMngr.OpenContext())
        {    
            GameConsole console = context.Consoles.FirstOrDefault(c => c.Name == consoleName);
        
            if (console != null)
            {
                Settings lastSetting = context.SettingCollection.FirstOrDefault(set => set.Key == SettingsConstants.LAST_CONSOLE);
                lastSetting.ValueInt = console.ID;
        
                context.SaveChanges();
        
                EvtDispatcher.BroadcastMessage($"Switched the current console to \"{consoleName}\"!");
            }
            else    
            {
                EvtDispatcher.BroadcastMessage($"No console named \"{consoleName}\" exists - cannot switch!");
            }
        }
        
        string rawName = RawGameNames[curIndex];
        
        //Wait a little bit before starting in case the pkill from above is still executing
        Thread.Sleep(500);
        
        EvtDispatcher.BroadcastMessage($"Booting up {rawName} - next game in {switchTimespan.TotalHours} hour(s) at {nextGameTimeUTC} UTC!");
        
        //Change max character count here
        const int maxCharCount = 35;
        string thing = Helpers.SplitStringWithinCharCount($"{rawName}", maxCharCount, " ", out List<string> textList);
        
        if (textList != null)
        {
            thing = string.Empty;
            for (int i = 0; i < textList.Count; i++)
            {
                thing += textList[i];
                if (i < (textList.Count - 1))
                {
                    thing += '\n';
                }
            }
        }
        
        //Set this file to display
        if (string.IsNullOrEmpty(GAME_TEXT_FILE_NAME) == false)
        {
            FileHelpers.SaveToTextFile(GAME_TEXT_FILE_NAME, thing);
        }
        
        if (string.IsNullOrEmpty(START_TIME_TEXT_FILE_NAME) == false)
        {
            FileHelpers.SaveToTextFile(START_TIME_TEXT_FILE_NAME, $"Started: {currentTimeUTC.ToShortDateString()} {currentTimeUTC.ToShortTimeString()}");
        }
        
        using (Process retroArchProcess = RunGame(curCore, curGame))
        {
            
        }
        
        //Increment counter
        using (BotDBContext context = DatabaseMngr.OpenContext())
        {
            Settings gameRotSetting = context.GetSetting(ROTATION_INDEX_SETTING);
            gameRotSetting.ValueInt += 1L;
            
            context.SaveChanges();
        }
    }
    
    private Process RunGame(string coreName, string gameName)
    {
        //Logger.Information($"{SCRIPT_TO_RUN} -L \"{coreName}\" \"{gameName}\"");
        
        return Process.Start(SCRIPT_TO_RUN, $"-L \"{coreName}\" \"{gameName}\"");
    }
}

return new GameRotationRoutine();
