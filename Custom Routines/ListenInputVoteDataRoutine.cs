/* ListenInputVoteDataRoutine
   - A routine that listens to input vote data and exports it to a text file
   - Useful for displaying this information in OBS!
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Diagnostics;
using TRBot.Connection;
using TRBot.Data;
using TRBot.Input;
using TRBot.Input.Consoles;
using TRBot.Utilities;
using TRBot.Routines;
using Newtonsoft.Json;
using static TRBot.Data.DataEventObjects;

public class ListenInputVoteDataRoutine : BaseRoutine
{
    //Choose the path and name of the text file to save into (Ex. "/home/user/Desktop/DemocracyData.txt")
    private const string TEXT_FILE_NAME = "";

    //If true, the data should be updated in the text file
    private bool MarkedDirty = false;

    //How long to check to save the text file if the data is dirty and should be updated
    private double DirtyCheckMilliseconds = 500d; 

    private DateTime NextUpdateTime = DateTime.UnixEpoch;

    private InputModes InputMode = InputModes.Anarchy;
    private Dictionary<string, int> InputVotesAndCounts = new Dictionary<string, int>();

    public ListenInputVoteDataRoutine()
    {

    }

    public override void Initialize()
    {
        base.Initialize();

        InputModes inputMode = (InputModes)DatabaseMngr.GetSettingInt(SettingsConstants.INPUT_MODE, (long)InputModes.Anarchy);
        InputMode = inputMode;

        EvtDispatcher.RegisterEvent<InputVoteArgs>(RoutineEventNames.INPUT_VOTE);
        EvtDispatcher.RegisterEvent<InputVoteEndArgs>(RoutineEventNames.INPUT_VOTE_END);
        EvtDispatcher.RegisterEvent<EvtDataReloadedArgs>(DataEventNames.RELOAD_DATA);

        EvtDispatcher.RemoveListener<InputVoteArgs>(RoutineEventNames.INPUT_VOTE, HandleInputVote);
        EvtDispatcher.AddListener<InputVoteArgs>(RoutineEventNames.INPUT_VOTE, HandleInputVote);

        EvtDispatcher.RemoveListener<InputVoteEndArgs>(RoutineEventNames.INPUT_VOTE_END, HandleInputVoteEnd);
        EvtDispatcher.AddListener<InputVoteEndArgs>(RoutineEventNames.INPUT_VOTE_END, HandleInputVoteEnd);

        EvtDispatcher.RemoveListener<EvtDataReloadedArgs>(DataEventNames.RELOAD_DATA, OnDataReloaded);
        EvtDispatcher.AddListener<EvtDataReloadedArgs>(DataEventNames.RELOAD_DATA, OnDataReloaded);

        NextUpdateTime = DateTime.UtcNow + TimeSpan.FromMilliseconds(DirtyCheckMilliseconds);
    }

    public override void CleanUp()
    {
        base.CleanUp();

        EvtDispatcher.RemoveListener<InputVoteArgs>(RoutineEventNames.INPUT_VOTE, HandleInputVote); 
        EvtDispatcher.RemoveListener<InputVoteEndArgs>(RoutineEventNames.INPUT_VOTE_END, HandleInputVoteEnd);
        EvtDispatcher.RemoveListener<EvtDataReloadedArgs>(DataEventNames.RELOAD_DATA, OnDataReloaded);

        EvtDispatcher.UnregisterEvent(RoutineEventNames.INPUT_VOTE);
        EvtDispatcher.UnregisterEvent(RoutineEventNames.INPUT_VOTE_END);
        EvtDispatcher.UnregisterEvent(DataEventNames.RELOAD_DATA);
    }
    
    private void OnDataReloaded(EvtDataReloadedArgs args)
    {
        InputModes inputMode = (InputModes)DatabaseMngr.GetSettingInt(SettingsConstants.INPUT_MODE, (long)InputModes.Anarchy);
        
        if (inputMode != InputMode)
        {
            InputMode = inputMode;
            MarkedDirty = true;
        }
    }

    private void HandleInputVote(InputVoteArgs args)
    {
        if (InputVotesAndCounts.TryGetValue(args.InputStr, out int voteCount) == false)
        {
            InputVotesAndCounts.Add(args.InputStr, 0);
        }

        InputVotesAndCounts[args.InputStr] += 1;

        MarkedDirty = true;
    }

    private void HandleInputVoteEnd(InputVoteEndArgs args)
    {
        InputVotesAndCounts.Clear();

        MarkedDirty = true;
    }

    public override void UpdateRoutine(in DateTime currentTimeUTC)
    {
        if (MarkedDirty == false)
        {
            return;
        }

        TimeSpan diff = currentTimeUTC - NextUpdateTime;

        if (diff.TotalMilliseconds < DirtyCheckMilliseconds)
        {
            return;
        }

        //Sort by value descending
        List<KeyValuePair<string, int>> sortedList = InputVotesAndCounts.OrderByDescending(kv => kv.Value).ToList();

        StringBuilder stringBuilder = new StringBuilder(500);

        stringBuilder.Append(InputMode.ToString()).Append(Environment.NewLine).Append(Environment.NewLine);
        
        for (int i = 0; i < sortedList.Count; i++)
        {
            stringBuilder.Append('"').Append(sortedList[i].Key).Append("\": ").Append(sortedList[i].Value.ToString());
            if (i < sortedList.Count - 1)
            {
                stringBuilder.Append(Environment.NewLine);
            }
        }

        string voteData = stringBuilder.ToString();

        if (string.IsNullOrEmpty(TEXT_FILE_NAME) == false)
        {
            bool saved = FileHelpers.SaveToTextFile(TEXT_FILE_NAME, voteData);
            if (saved == false)
            {
                EvtDispatcher.BroadcastMessage("Failed saving vote data to text file.");
            }
        }

        NextUpdateTime = currentTimeUTC + TimeSpan.FromMilliseconds(DirtyCheckMilliseconds);
        MarkedDirty = false;
    }
}

return new ListenInputVoteDataRoutine();